<?php

/**
 * Settings form.
 */
function node_link_stat_settings_form() {
  $form['node_link_stat_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable log access'),
    '#default_value' => variable_get('node_link_stat_enabled', FALSE),
  );
  $types = node_get_types('types');
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['node_link_stat_ctypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content types'),
    '#options' => $options,
    '#default_value' => variable_get('node_link_stat_ctypes', array()),
  );
  return system_settings_form($form);
}
