<?php

/**
 * Export as CSV file.
 */
function node_link_stat_export($node = NULL) {
  // List.
  $array = array();
  if ($node) {
    $data = _get_node_link_stat_list($node, TRUE);
    $header[] = array(t('URL'), t('Clicked'), t('Last access'));
    $data['rows'] = array_merge($header, $data['rows']);
    $filename = 'report-'. $node->nid;
  }
  // Overview.
  else {
    $data = _get_node_link_stat_overview(TRUE);
    $header[] = array(t('Node title'), t('Node URL'), t('Total clicked'));
    $data['rows'] = array_merge($header, $data['rows']);
    $filename = 'report';
  }
  $output = array_to_csv($data['rows']);
  drupal_set_header('Content-Type: application/csv');
  drupal_set_header('Content-Disposition: attachment; filename='. $filename .'.csv');
  drupal_set_header('Pragma: no-cache');
  print $output;
}
