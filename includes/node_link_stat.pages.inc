<?php

/**
 * Log access statistic.
 */
function node_link_stat_log($node) {
  if (valid_url($_GET['url'], TRUE)) {
    if (node_link_stat_allowed($node)) {
      $insert = array(
        'columns' => array(
          'nid' => '%d',
          'url' => "'%s'",
          'hostname' => "'%s'",
          'timestamp' => '%d',
        ),
        'values' => array(
          $node->nid,
          $_GET['url'],
          ip_address(),
          time(),
        ),
      );
      $columns = implode(', ', array_keys($insert['columns']));
      $values = implode(', ', array_values($insert['columns']));
      db_query("INSERT INTO {node_link_statistic} ($columns) VALUES ($values)", $insert['values']);
    }
    drupal_goto($_GET['url']);
  }
  else {
    drupal_goto('<front>');
  }
}

/**
 * Report overview.
 */
function node_link_stat_overview() {
  $data = _get_node_link_stat_overview();
  if (empty($data['rows'])) {
    $data['rows'][] = array(
      array(
        'colspan' => count($data['header']),
        'data' => t('Log access is empty.'),
      ),
    );
  }
  $output = l(t('Export to CSV'), $_GET['q'].'/export');
  $output .= theme('table', $data['header'], $data['rows']);
  return $output;
}

/**
 * Report each node.
 */
function node_link_stat_list($node) {
  $data = _get_node_link_stat_list($node);
  if (empty($data['rows'])) {
    $data['rows'][] = array(
      array(
        'colspan' => count($data['header']),
        'data' => t('Log access for nid: !nid is empty.', array('!nid' => $node->nid)),
      ),
    );
  }
  $output = l(t('Export to CSV'), $_GET['q'].'/export');
  $output .= theme('table', $data['header'], $data['rows']);
  $output .= '<br />'. l(t('Back'), 'node_link_stat');
  return $output;
}
